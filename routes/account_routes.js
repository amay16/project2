var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');

router.get('/home', function(req, res) {

    res.render('accounts/accountsHome', { 'result':res });


});

router.get('/search', function(req, res) {
    account_dal.search(req.query.accounts_search, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('accounts/accountsSearch', { 'result':result, 'search':req.query });
        }
    });

});

// View All movies
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('accounts/accountsViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.email == "") {
        res.send('email is null');
    }
    else {
        account_dal.getById(req.query.email, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('accounts/accountsViewById', {'account': result[0][0], 'movies_watched': result[1],
               'movies_on_watchlist': result[2], 'tv_shows_watched': result[3], 'tv_shows_on_watchlist': result[4]});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually

    account_dal.getEverything( function(err, result) {
        res.render('accounts/accountsAdd', {'Movies': result[0], 'Shows': result[1] });
    });

});

router.get('/editRating', function(req, res) {

});

// View the company for the given id
router.post('/insert', function(req, res) {
    // simple validation
    if (req.body.email == "") {
        res.send('email must be provided.');
    }
    else if (req.body.username == "") {
        res.send('username must be provided.');
    }
    else if ( req.body.fname == "" ) {
        res.send('first name must be specified')
    }
    else if (req.body.lname == "") {
        res.send('last name must be specified');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(req.body, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {


                account_dal.getAll(function(err, resh){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('accounts/accountsViewAll', { 'result':resh, 'was_successful':true, 'newa': req.body });
                    }
                });


            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.company_id == null) {
        res.send('A company id is required');
    }
    else {
        company_dal.edit(req.query.company_id, function(err, result){
            res.render('company/companyUpdate', {company: result[0][0], compAddress: result[1], address: result[2]});
        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});

router.get('/update', function(req, res) {
    company_dal.update(req.query, function(err, result){
       res.redirect(302, '/company/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.username == null) {
        res.send('username is null');
    }
    else {
         account_dal.delete(req.query.username, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/accounts/all');
             }
         });
    }
});

module.exports = router;
