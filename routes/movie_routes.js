var express = require('express');
var router = express.Router();
var movie_dal = require('../model/movie_dal');

router.get('/home', function(req, res) {

    res.render('movies/moviesHome', { 'result':res });


});

router.get('/search', function(req, res) {
    movie_dal.search(req.query.movie_search, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('movies/moviesSearch', { 'result':result, 'search':req.query });
        }
    });

});

// View All movies
router.get('/all', function(req, res) {
    movie_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('movies/moviesViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.Movie_ID == null) {
        res.send('Movie_ID is null');
    }
    else {
        movie_dal.getById(req.query.Movie_ID, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('movies/moviesViewById', {'result': result, 'Movie_ID': req.query.Movie_ID});
           }
        });
    }
});

router.get('/moviesAddActor', function(req, res){
    if(req.query.Movie_ID == null) {
        res.send('Movie_ID is null');
    }
    else if( req.query.Movie_Title == null ) {
        res.send('Movie_Title is null')
    }
    else {
        movie_dal.getActors( function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('movies/moviesAddActors', {'actors': result, 'Movie_ID': req.query.Movie_ID,
                                                        'Movie_Title': req.query.Movie_Title});
            }
        });
    }
});

router.post('/moviesSelectActor', function(req, res) {
    if(req.body.Movie_ID == null) {
        res.send('Movie_ID is null');
    }
    else if( req.body.Movie_Title == null ) {
        res.send('Movie_Title is null')
    }
    else {
        movie_dal.getActorInfo(req.body.Actor_ID, function (err, result) {
            res.render('movies/moviesSelectActors', {
                'result': req.body, 'actors': result,
                'Movie_ID': req.body.Movie_ID, 'Movie_Title': req.body.Movie_Title, 'Actor_ID': req.body.Actor_ID
            })
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually

    var ratings = ["G", "PG", "PG-13", "R", "NC-17", "NR"];

    movie_dal.getActors( function(err, result) {
        res.render('movies/moviesAdd', {'result': res, 'ratings': ratings, 'actors': result});
    });

});

router.post('/insertAct', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually

    for( i = 0; i < req.body.Actor_ID.length; i++ ) {
        if( req.body.Character_Name[i] == "" ) {
            res.send('Character Name must be provided')
        }
    }

    movie_dal.insertMovAct( req.body.Actor_ID, req.body.Movie_ID, req.body.Character_Name, function() {
        movie_dal.getAll( function(err, resh) {
            res.render('movies/moviesViewAll', {'result': resh, 'newm': req.body.Movie_Title, 'was_successful': true});
        })

    });

});

// View the company for the given id
router.post('/insert', function(req, res) {
    // simple validation
    if (req.body.Movie_Title == "") {
        res.send('Movie Title must be provided.');
    }
    else if (req.body.Date_Released == "") {
        res.send('Date Released must be provided.');
    }
    else if ( req.body.Runtime == null ) {
        res.send('Runtime must be provided')
    }
    else if (req.body.Ratings == "") {
        res.send('At least one address must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        movie_dal.insert(req.body, function(err,Movie_ID) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else if ( req.body.Actor_ID == null ) {
                res.send('You must choose some actors')
            }
            else {
                movie_dal.getActorInfo( req.body.Actor_ID, function( err, result ) {
                    res.render('movies/moviesSelectActors', { 'result': req.body, 'actors': result,
                        'Movie_ID': Movie_ID, 'Movie_Title': req.body.Movie_Title, 'Actor_ID': req.body.Actor_ID })
                });


            }
        });
    }
});

router.get('/avgscore', function( req, res ) {

    movie_dal.getAvgScore( req.query.rating, function( err, result ) {
        res.render('movies/moviesAvgScore', { 'result':result });
    });

});

router.get('/top10', function( req, res ) {

    movie_dal.getTop10( function( err, result ) {
        res.render('movies/moviesAvgScore', { 'result':result });
    });

});

router.get('/edit', function(req, res){
    if(req.query.company_id == null) {
        res.send('A company id is required');
    }
    else {
        company_dal.edit(req.query.company_id, function(err, result){
            res.render('company/companyUpdate', {company: result[0][0], compAddress: result[1], address: result[2]});
        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});

router.get('/update', function(req, res) {
    company_dal.update(req.query, function(err, result){
       res.redirect(302, '/company/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.Movie_ID == null) {
        res.send('Movie_ID is null');
    }
    else {
         movie_dal.delete(req.query.Movie_ID, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/movies/all');
             }
         });
    }
});

router.get('/deleteA', function(req, res){
    if(req.query.Actor_ID == null) {
        res.send('Actor_ID is null');
    }
    else if( req.query.Movie_ID == null ) {
        res.send('Movie_ID is null')
    }
    else {
        movie_dal.deleteA(req.query.Actor_ID, req.query.Movie_ID, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/movies/all');
            }
        });
    }
});

module.exports = router;
