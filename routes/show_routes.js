var express = require('express');
var router = express.Router();
var show_dal = require('../model/show_dal');

router.get('/home', function(req, res) {

    res.render('shows/showsHome', { 'result':res });


});

router.get('/search', function(req, res) {
    show_dal.search(req.query.show_search, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('shows/showsSearch', { 'result':result, 'search':req.query });
        }
    });

});

// View All movies
router.get('/all', function(req, res) {
    show_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('shows/showsViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.Show_ID == null) {
        res.send('Show_ID is null');
    }
    else {
        show_dal.getById(req.query.Show_ID, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('shows/showsViewById', {'result': result});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually

    var airing = ["No", "Yes"];

    show_dal.getActors( function(err, result) {
        res.render('shows/showsAdd', {'result': res, 'airing': airing, 'actors': result});
    });

});

router.post('/insertAct', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually

    for( i = 0; i < req.body.Actor_ID.length; i++ ) {
        if( req.body.Character_Name[i] == "" ) {
            res.send('Character Name must be provided')
        }
    }

    show_dal.insertMovAct( req.body.Actor_ID, req.body.Show_ID, req.body.Character_Name, function() {
        show_dal.getAll( function(err, resh) {
            res.render('shows/showsViewAll', {'result': resh, 'newm': req.body.Show_Title, 'was_successful': true});
        })

    });

});

// View the company for the given id
router.post('/insert', function(req, res) {
    // simple validation
    if (req.body.Show_Title == "") {
        res.send('Movie Title must be provided.');
    }
    else if (req.body.Currently_Airing == null) {
        res.send('Currently Airing must be provided.');
    }
    else if ( req.body.Year_Start == null ) {
        res.send('Year start must be specified')
    }
    else if (req.body.Year_End == null) {
        res.send('Year end must be specified');
    }
    else if (req.body.Num_Episodes == null) {
        res.send('Num episodes must be specified');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        show_dal.insert(req.body, function(err,Show_ID) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else if ( req.body.Actor_ID == null ) {
                res.send('You must choose some actors')
            }
            else {
                show_dal.getActorInfo( req.body.Actor_ID, function( err, result ) {
                    res.render('shows/showsSelectActors', { 'result': req.body, 'actors': result,
                        'Show_ID': Show_ID, 'Show_Title': req.body.Show_Title, 'Actor_ID': req.body.Actor_ID })
                });


            }
        });
    }
});

router.get('/avgscore', function( req, res ) {

    show_dal.getAvgScore( req.query.rating, function( err, result ) {
        res.render('shows/showsAvgScore', { 'result':result });
    });

});

router.get('/top10', function( req, res ) {

    show_dal.getTop10( function( err, result ) {
        res.render('shows/showsAvgScore', { 'result':result });
    });

});

router.get('/edit', function(req, res){
    if(req.query.company_id == null) {
        res.send('A company id is required');
    }
    else {
        company_dal.edit(req.query.company_id, function(err, result){
            res.render('company/companyUpdate', {company: result[0][0], compAddress: result[1], address: result[2]});
        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});

router.get('/update', function(req, res) {
    company_dal.update(req.query, function(err, result){
       res.redirect(302, '/company/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.Show_ID == null) {
        res.send('Show_ID is null');
    }
    else {
         show_dal.delete(req.query.Show_ID, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/shows/all');
             }
         });
    }
});

module.exports = router;
