var express = require('express');
var router = express.Router();
var actor_dal = require('../model/actor_dal');

router.get('/home', function(req, res) {

    res.render('actors/actorsHome', { 'result':res });


});

router.get('/search', function(req, res) {
    actor_dal.search(req.query.actor_search, function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('actors/actorsSearch', { 'result':result, 'search':req.query });
        }
    });

});

// View All movies
router.get('/all', function(req, res) {
    actor_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('actors/actorsViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.Actor_ID == null) {
        res.send('Actor_ID is null');
    }
    else {
        actor_dal.getById(req.query.Actor_ID, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('actors/actorsViewById', {'actor': result[0][0], 'movies': result[1], 'shows': result[2]});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually

    var gender = ["Female", "Male"];

    actor_dal.getActors( function(err, result) {
        res.render('actors/actorsAdd', {'result': res, 'gender': gender, 'actors': result});
    });

});

/*
router.post('/insertAct', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually

    for( i = 0; i < req.body.Actor_ID.length; i++ ) {
        if( req.body.Character_Name[i] == "" ) {
            res.send('Character Name must be provided')
        }
    }

    show_dal.insertMovAct( req.body.Actor_ID, req.body.Show_ID, req.body.Character_Name, function() {
        show_dal.getAll( function(err, resh) {
            res.render('shows/showsViewAll', {'result': resh, 'newm': req.body.Show_Title, 'was_successful': true});
        })

    });

});
*/

// View the company for the given id
router.post('/insert', function(req, res) {
    // simple validation
    if (req.body.fname == "") {
        res.send('First Name must be provided.');
    }
    else if (req.body.lname == "") {
        res.send('Last Name must be provided.');
    }
    else if ( req.body.DOB == null ) {
        res.send('Date of Birth must be specified')
    }
    else if (req.body.gender == null) {
        res.send('Gender must be specified');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        actor_dal.insert(req.body, function(err,resh) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                actor_dal.getAll( function(err, result) {
                    res.render('actors/actorsViewAll', { 'result': result, 'newa': req.body, 'was_successful': true })
                });




                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                /*movie_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('movies/moviesViewAll', { 'result':result, 'newm': Movie_Title,
                            was_successful: true });
                    }
                });*/

            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.company_id == null) {
        res.send('A company id is required');
    }
    else {
        company_dal.edit(req.query.company_id, function(err, result){
            res.render('company/companyUpdate', {company: result[0][0], compAddress: result[1], address: result[2]});
        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});

router.get('/update', function(req, res) {
    company_dal.update(req.query, function(err, result){
       res.redirect(302, '/company/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.Actor_ID == null) {
        res.send('Actor_ID is null');
    }
    else {
         actor_dal.delete(req.query.Actor_ID, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/actors/all');
             }
         });
    }
});

module.exports = router;
