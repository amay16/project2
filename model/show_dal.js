var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.search = function(show_search, callback) {
    var query = 'SELECT * FROM TV_Shows WHERE Show_Title LIKE \'%\' ?  \'%\' ';

    var queryData = [show_search];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getAvgScore = function( rating, callback ) {
    var query = 'select Show_ID, Show_Title, IFNULL(fn_avg_show_score(Show_Title), 0) AS Average_Rating FROM TV_Shows ' +
        'HAVING Average_Rating > ? ' +
        'ORDER BY Average_Rating DESC';

    connection.query(query, rating, function( err, result ) {
        callback(err, result);
    });
};

exports.getTop10 = function( callback ) {
    var query = 'select Show_ID, Show_Title, IFNULL(fn_avg_show_score(Show_Title), 0) AS Average_Rating FROM TV_Shows ' +
        'ORDER BY Average_Rating DESC ' +
        'limit 10;';

    connection.query(query, function( err, result ) {
        callback(err, result);
    });
};

exports.getActorInfo = function(ActorIDs, callback) {
    var query = 'SELECT * FROM Actors WHERE Actor_ID = (?)';

    for( var i = 0; i < ActorIDs.length - 1; i++ ) {
        query += ' or Actor_ID = (?)'
    }


    connection.query(query, ActorIDs, function(err, result) {
        callback(err, result);
    });
};

exports.insertMovAct = function(Actor_ID, Show_ID, Character_Name, callback) {
    var query = 'INSERT INTO Actor_TV_Shows_Starred_In (Actor_ID, Show_ID, Character_Name) VALUES ?';

    // Build actor ID array
    var actorA = [];
    for( var j = 0; j < Actor_ID.length; j++ ) {
        if( Actor_ID[j] != "," ) {
            actorA.push(Actor_ID[j]);
        }
    }

    // Build queries
    var queryData = [];
    for(var i=0; i < actorA.length; i++) {
        queryData.push([actorA[i], Show_ID, Character_Name[i]]);
    }
    

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};



exports.getAll = function(callback) {
    var query = 'SELECT * FROM TV_Shows ' +
        'ORDER BY Show_Title asc;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getActors = function(callback) {
    var query = 'SELECT * FROM Actors;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(Show_ID, callback) {
    var query = 'SELECT * FROM TV_Shows tv ' +
                'LEFT JOIN Actor_TV_Shows_Starred_In atv ON tv.Show_ID = atv.Show_ID ' +
                'LEFT JOIN Actors a ON a.Actor_ID = atv.Actor_ID ' +
                'WHERE tv.Show_ID = ?';

    var queryData = [Show_ID];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO TV_Shows (Show_Title, Currently_Airing, Year_Start, Year_End, Num_Episodes) VALUES (?)';

    var queryData = [params.Show_Title, params.Currently_Airing, params.Year_Start, params.Year_End, params.Num_Episodes];

    connection.query(query, [queryData], function(err, result) {

        var Show_ID = result.insertId;

        callback(err, Show_ID);

    });

};

exports.delete = function(Show_ID, callback) {
    var query = 'DELETE FROM TV_Shows WHERE Show_ID = ?';
    var queryData = [Show_ID];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    for(var i=0; i < addressIdArray.length; i++) {
        companyAddressData.push([company_id, addressIdArray[i]]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE company SET company_name = ? WHERE company_id = ?';
    var queryData = [params.company_name, params.company_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        companyAddressDeleteAll(params.company_id, function(err, result){

            if(params.address_id != null) {
                //insert company_address ids
                companyAddressInsert(params.company_id, params.address_id, function(err, result){
                    callback(err, result);
                });
            }
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS company_getinfo;

     DELIMITER //
     CREATE PROCEDURE company_getinfo (company_id int)
     BEGIN

     SELECT * FROM company WHERE company_id = _company_id;

     SELECT a.*, s.company_id FROM address a
     LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL company_getinfo (4);

 */

exports.edit = function(company_id, callback) {
    var query = 'CALL company_getinfo(?)';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};